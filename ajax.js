(function ($) {

  $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
  $('#btnAjax').click(function () { callRestAPI() });

  // Perform an asynchronous HTTP (Ajax) API request.
  function callRestAPI() {
    var root = 'https://jsonplaceholder.typicode.com';
    var postNum=Math.ceil(Math.random()*100);
    $.ajax({
      url: root + '/posts/'+postNum,
      method: 'GET'
    }).then(function (response) {
      console.log(response);
      $('#showResult').html(response.body);
    });
  }
})(jQuery);